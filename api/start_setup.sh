#!/bin/sh

sleep 1m

GITLAB_ACCESS_TOKEN=''
GITLAB_SETUP_PROJECT_ID=''

curl -L \
  -o /home/ubuntu/setup.tar.gz \
  -H "Private-Token: $GITLAB_ACCESS_TOKEN" \
  "https://gitlab.com/api/v4/projects/$GITLAB_SETUP_PROJECT_ID/repository/archive.tar.gz"
mkdir /home/ubuntu/setup
tar -xzf /home/ubuntu/setup.tar.gz -C /home/ubuntu/setup --strip-components=1

GITLAB_ACCESS_TOKEN=$GITLAB_ACCESS_TOKEN \
  DOMAIN='' \
  DOCKER_DOMAIN='registry.gitlab.com' \
  DOCKER_ACCESS_TOKEN=$GITLAB_ACCESS_TOKEN \
  DOCKER_USERNAME='' \
  DOCKER_REPO='registry.gitlab.com/<gitlab-username>/scaffold-core' \
  DOCKER_TAG='' \
  DOCKER_ENTRYPOINT='' \
  SCAFFOLD_API_WEBMASTER_EMAIL='' \
  SCAFFOLD_API_AUTH_KEY='' \
  SCAFFOLD_API_SECRET_KEY_BASE='' \
  SCAFFOLD_API_MYSQL_DB_HOST='' \
  SCAFFOLD_API_MYSQL_DB_PORT='' \
  SCAFFOLD_API_MYSQL_DB='' \
  SCAFFOLD_API_MYSQL_USERNAME='' \
  SCAFFOLD_API_MYSQL_PASSWORD='' \
  /bin/sh /home/ubuntu/setup/api/setup.sh

rm -r /home/ubuntu/setup.tar.gz /home/ubuntu/setup
