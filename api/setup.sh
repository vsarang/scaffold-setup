#!/bin/sh

add-apt-repository ppa:certbot/certbot -y
apt update
apt install -y nginx certbot python-certbot-nginx docker.io

rm -f /etc/nginx/sites-available/* /etc/nginx/sites-enabled/*
cp /home/ubuntu/setup/api/nginx.template.conf /etc/nginx/conf.d/$DOMAIN.conf
sed "s~<domain>~$DOMAIN~g" -i /etc/nginx/conf.d/$DOMAIN.conf
mkdir /var/www/scaffold-api
service nginx restart

cp /home/ubuntu/setup/api/certbot.template.ini /home/ubuntu/setup/api/certbot.ini
sed "s~<domain>~$DOMAIN~g" -i /home/ubuntu/setup/api/certbot.ini
sed "s~<certbot-email>~$SCAFFOLD_API_WEBMASTER_EMAIL~g" -i /home/ubuntu/setup/api/certbot.ini
certbot -c /home/ubuntu/setup/api/certbot.ini
rm -f /home/ubuntu/setup/api/certbot.ini

mkdir /home/ubuntu/scaffold

touch /home/ubuntu/scaffold/.scaffold-api-env
echo 'RAILS_ENV=production' >> /home/ubuntu/scaffold/.scaffold-api-env
echo "SCAFFOLD_API_AUTH_KEY=$SCAFFOLD_API_AUTH_KEY" >> /home/ubuntu/scaffold/.scaffold-api-env
echo "SCAFFOLD_API_SECRET_KEY_BASE=$SCAFFOLD_API_SECRET_KEY_BASE" >> /home/ubuntu/scaffold/.scaffold-api-env
echo "SCAFFOLD_API_MYSQL_DB_HOST=$SCAFFOLD_API_MYSQL_DB_HOST" >> /home/ubuntu/scaffold/.scaffold-api-env
echo "SCAFFOLD_API_MYSQL_DB_PORT=$SCAFFOLD_API_MYSQL_DB_PORT" >> /home/ubuntu/scaffold/.scaffold-api-env
echo "SCAFFOLD_API_MYSQL_DB=$SCAFFOLD_API_MYSQL_DB" >> /home/ubuntu/scaffold/.scaffold-api-env
echo "SCAFFOLD_API_MYSQL_USERNAME=$SCAFFOLD_API_MYSQL_USERNAME" >> /home/ubuntu/scaffold/.scaffold-api-env
echo "SCAFFOLD_API_MYSQL_PASSWORD=$SCAFFOLD_API_MYSQL_PASSWORD" >> /home/ubuntu/scaffold/.scaffold-api-env

cp /home/ubuntu/setup/api/update.template.sh /home/ubuntu/scaffold/update.sh
sed "s~<docker-repo>~$DOCKER_REPO~g" -i /home/ubuntu/scaffold/update.sh
sed "s~<docker-tag>~$DOCKER_TAG~g" -i /home/ubuntu/scaffold/update.sh
sed "s~<docker-entrypoint>~$DOCKER_ENTRYPOINT~g" -i /home/ubuntu/scaffold/update.sh

chmod -R o-rwx /home/ubuntu/scaffold
chown -R ubuntu /home/ubuntu/scaffold
chgrp -R ubuntu /home/ubuntu/scaffold

sudo usermod -aG docker ubuntu
echo $DOCKER_ACCESS_TOKEN | sudo -u ubuntu docker login $DOCKER_DOMAIN --username $DOCKER_USERNAME --password-stdin

sudo -u ubuntu /bin/sh /home/ubuntu/scaffold/update.sh
