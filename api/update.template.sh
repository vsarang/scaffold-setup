#!/bin/sh

REPOSITORY='<docker-repo>'
TAG='<docker-tag>'
RUN_ARGS='--env-file /home/ubuntu/scaffold/.scaffold-api-env -v /var/www/scaffold-api/:/var/www/scaffold-api/ -p 3000:3000 --entrypoint <docker-entrypoint>'

IMAGE_NAME=$REPOSITORY:$TAG
CONTAINER_NAME=`echo $REPOSITORY-$TAG | sed 's~/~-~g'`

echo "docker stop $CONTAINER_NAME"
docker stop $CONTAINER_NAME || true
echo "docker rm $CONTAINER_NAME"
docker rm $CONTAINER_NAME || true
echo "docker image rm $IMAGE_NAME"
docker image rm $IMAGE_NAME || true

echo "docker pull $IMAGE_NAME"
docker pull $IMAGE_NAME
echo "docker run -d --name $CONTAINER_NAME $RUN_ARGS $IMAGE_NAME"
docker run -d --name $CONTAINER_NAME $RUN_ARGS $IMAGE_NAME
