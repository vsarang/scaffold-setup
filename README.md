## `start_setup.sh`

### `GITLAB_ACCESS_TOKEN`

From the [GitLab User Settings page](https://gitlab.com/-/profile/personal_access_tokens), create a
new Personal Access Token with the following scopes:

- read_api
- read_registry

### `DOMAIN`

The location of the webserver being set up.

### `DOCKER_USERNAME`

The user account that will be used to download server images.

### `DOCKER_TAG`

The tag that will be used when downloading server images.

### `DOCKER_ENTRYPOINT`

Passed to `docker run` as the `--entrypoint`.
