#!/bin/sh

add-apt-repository ppa:certbot/certbot -y
apt update
apt install -y nginx certbot python-certbot-nginx

cp /home/ubuntu/setup/apex/nginx.template.conf /home/ubuntu/setup/apex/nginx.conf
sed "s~<domain>~$DOMAIN~g" -i /home/ubuntu/setup/apex/nginx.conf

rm -f /etc/nginx/sites-available/* /etc/nginx/sites-enabled/*
cp /home/ubuntu/setup/apex/nginx.conf /etc/nginx/conf.d/$DOMAIN.conf
service nginx restart

cp /home/ubuntu/setup/apex/certbot.template.ini /home/ubuntu/setup/apex/certbot.ini
sed "s~<domain>~$DOMAIN~g" -i /home/ubuntu/setup/apex/certbot.ini
sed "s~<certbot-email>~$CERTBOT_EMAIL~g" -i /home/ubuntu/setup/apex/certbot.ini
certbot -c /home/ubuntu/setup/apex/certbot.ini

rm -f \
  /home/ubuntu/setup/apex/nginx.conf \
  /home/ubuntu/setup/apex/certbot.ini
