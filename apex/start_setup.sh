#!/bin/sh

sleep 1m

GITLAB_ACCESS_TOKEN=''
GITLAB_SETUP_PROJECT_ID=''

curl -L \
  -o /home/ubuntu/setup.tar.gz \
  -H "Private-Token: $GITLAB_ACCESS_TOKEN" \
  "https://gitlab.com/api/v4/projects/$GITLAB_SETUP_PROJECT_ID/repository/archive.tar.gz"
mkdir /home/ubuntu/setup
tar -xzf /home/ubuntu/setup.tar.gz -C /home/ubuntu/setup --strip-components=1

GITLAB_ACCESS_TOKEN=$GITLAB_ACCESS_TOKEN \
  CERTBOT_EMAIL= \
  DOMAIN= \
  /bin/sh /home/ubuntu/setup/apex/setup.sh

rm -r /home/ubuntu/setup.tar.gz /home/ubuntu/setup
